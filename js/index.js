$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    //Scripts carousel
    $('.carousel').carousel({
      interval:2000
    });
    //Scripts del modal
    $('#contacto').on('show.bs.modal', function (e){
      console.log('El modal se está mostrando');
      //Se cambia de color al darle clic, señal de que ya se revisó.
      $('#contactoBtn').removeClass('btn-modal');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled',true);
    });
    $('#contacto').on('shown.bs.modal', function (e){
      console.log('El modal se mostró');
    });
    $('#contacto').on('hide.bs.modal', function (e){
      console.log('El modal contacto se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function (e){
      console.log('El modal contacto se ocultó');
      $('#contactoBtn').prop('disabled',false);
      $('#contactoBtn').addClass('btn-modal');
      $('#contactoBtn').removeClass('btn-primary');
    });
  });
      
